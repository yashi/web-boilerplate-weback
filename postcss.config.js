

module.exports = {
 

  plugins: [
    'postcss-short',
    'postcss-import',
    // 'postcss-easings',
    'postcss-utilities',
    'postcss-preset-env',
    ['postcss-assets',{
      loadPaths: ['~fonts/', '~img/']
    }],
    ['postcss-font-magician', {
      foundries: 'custom bootstrap google',
      custom: {
        'mida': {
          variants: {
            normal: {
              100: {
                url: {
                  woff2: '~fonts/avertastd-thin-webfont.woff2',
                  woff: '~fonts/avertastd-thin-webfont.woff',
                }
              },
              300: {
                url: {
                  woff2: '~fonts/avertastd-light-webfont.woff2',
                  woff: '~fonts/avertastd-light-webfont.woff',
                  ttf: '~fonts/avertastd-light-webfont.ttf',
                }
              },
              400: {
                url: {
                  woff2: '~fonts/avertastd-regular-webfont.woff2',
                  woff: '~fonts/avertastd-regular-webfont.woff',
                  ttf: '~fonts/avertastd-regular-webfont.ttf'
                }
              },
              600: {
                url: {
                  woff2: '~fonts/avertastd-semibold-webfont.woff2',
                  woff: '~fonts/avertastd-semibold-webfont.woff',
                }
              },
              700: {
                url: {
                  woff2: '~fonts/avertastd-bold-webfont.woff2',
                  woff: '~fonts/avertastd-bold-webfont.woff'
                }
              },
              800: {
                url: {
                  woff2: '~fonts/avertastd-extrabold-webfont.woff2',
                  woff: '~fonts/avertastd-extrabold-webfont.woff'
                }
              },
              900: {
                url: {
                  woff2: '~fonts/avertastd-black-webfont.woff2',
                  woff: '~fonts/avertastd-black-webfont.woff',
                  ttf: '~fonts/avertastd-black-webfont.ttf'
                }
              },
            },
            italic: {
              100: {
                url: {
                  woff2: '~fonts/avertastd-thinitalic-webfont.woff2',
                  woff: '~fonts/avertastd-thinitalic-webfont.woff',
                }
              },
              300: {
                url: {
                  woff2: '~fonts/avertastd-lightitalic-webfont.woff2',
                  woff: '~fonts/avertastd-lightitalic-webfont.woff',
                  ttf: '~fonts/avertastd-lightitalic-webfont.ttf',
                }
              },
              400: {
                url: {
                  woff2: '~fonts/avertastd-regularitalic-webfont.woff2',
                  woff: '~fonts/avertastd-regularitalic-webfont.woff',
                }
              },
              600: {
                url: {
                  woff2: '~fonts/avertastd-semibolditalic-webfont.woff2',
                  woff: '~fonts/avertastd-semibolditalic-webfont.woff',
                }
              },
              700: {
                url: {
                  woff2: '~fonts/avertastd-bolditalic-webfont.woff2',
                  woff: '~fonts/avertastd-bolditalic-webfont.woff',
                }
              },
              800: {
                url: {
                  woff2: '~fonts/avertastd-extrabolditalic-webfont.woff2',
                  woff: '~fonts/avertastd-extrabolditalic-webfont.woff',
                  ttf: '~fonts/avertastd-extrabolditalic-webfont.ttf'
                }
              },
              900: {
                url: {
                  woff2: '~fonts/avertastd-blackitalic-webfont.woff2',
                  woff: '~fonts/avertastd-blackitalic-webfont.woff',
                  ttf: '~fonts/avertastd-blackitalic-webfont.ttf'
                }
              },
            }
          }
        },
        'Socicon': {
          variants: {
            normal: {
              
              400: {
                url: {
                  woff2: '~fonts/Socicon.woff2',
                  woff: '~fonts/Socicon.woff',
                  ttf: '~fonts/Socicon.ttf',
                  eot: '~fonts/Socicon.eot',
                  svg: '~fonts/Socicon.svg',
                }
              }
            }
          }
        },
        'social': {
          variants: {
            normal: {
              
              400: {
                url: {
                  woff2: '~fonts/SocialicoPlus-Plus.woff2',
                  woff: '~fonts/SocialicoPlus-Plus.woff',
                  ttf: '~fonts/SocialicoPlus-Plus.ttf',
                  eot: '~fonts/SocialicoPlus-Plus.eot',
                  otf: '~fonts/SocialicoPlus-Plus.otf',
                }
              }
            }
          }
        },
        'social-2': {
          variants: {
            normal: {
              
              400: {
                url: {
                  woff2: '~fonts/Socialico-Regular.woff2',
                  woff: '~fonts/Socialico-Regular.woff',
                  ttf: '~fonts/Socialico-Regular.ttf',
                  eot: '~fonts/Socialico-Regular.eot',
                  otf: '~fonts/Socialico-Regular.otf',
                }
              }
            }
          }
        },
        'lg': {
          variants: {
            normal: {
              
              400: {
                url: {
                  woff: '~fonts/lg.woff',
                  ttf: '~fonts/lg.ttf',
                  eot: '~fonts/lg.eot',
                  svg: '~fonts/lg.svg',
                }
              }
            }
          }
        }
      }
    }],
    ['autoprefixer', {
      remove: false,
      grid: true,
      flexbox: true,
      supports: true
    }],
    ['cssnano', {
      preset: ['advanced', {
        comments: {
            removeAll: true
        }
    }],
    }]
  ],


  
}