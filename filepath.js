let path = require('path');

module.exports = {
    inputPath : path.resolve(__dirname, './src/js/', 'index.js'),
    outputPath : path.resolve(__dirname, './', 'dist'),
    fileP : path.resolve(__dirname, './dist/', 'assets')
}

