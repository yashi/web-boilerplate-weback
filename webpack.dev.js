const path = require('path');

// Webpack Plugins 
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MediaQueryPlugin = require('media-query-plugin');
const CopyFiles = require('copy-webpack-plugin');
const WebpackBar = require('webpackbar');
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const fs = require('fs');
const webpages = JSON.parse(fs.readFileSync('./src/pages.json', 'utf8'));


const filepath = require('./filepath.js');
const argv = require("yargs").argv;
const isProduction = !!argv.production;


const entryHtmlPlugins = webpages.map(function (entryName) {
    return new HtmlWebpackPlugin({
      filename: entryName + '.html',
      template: __dirname + `/src/template/${entryName}.html`,
      inject: false,
      title:entryName,
    })
  });


module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: filepath.inputPath,
    output: {
        path: path.resolve(__dirname, './', 'dist'),
        filename: "bundle.js"
    },
    watch:true,
    devServer: {
        contentBase: filepath.outputPath,
        writeToDisk: false,
        publicPath: '/',
        port: 3000,
        overlay: {
            warnings: false,
            errors: true
        }
    },
    module: {
        rules: [{
                test: /\.(scss|css)$/,
                exclude: /node_modules/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            // minimize: true,
                            modules: true,
                            importLoaders: 1
                        }
                    },

                    {
                        loader: 'postcss-loader',
                        options: {
                            // execute: 'true',
                            postcssOptions: {
                                config: path.resolve(__dirname, 'postcss.config.js'),
                            },
                          },
                        
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require('node-sass'),
                            sassOptions: {
                                includePaths: [
                                    path.resolve(__dirname, 'node_modules/foundation-sites/scss/'),
                                    path.resolve(__dirname, 'node_modules/bootstrap/scss/')
                                ]
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: '/',
                        publicPath: '/',
                    },
                }, ],
            },
            {
                test: /\.(gif|png|jpe?g|svg|mp4|webm)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: '/img/',
                        publicPath: '/img/',
                    },
                }]
            }
        ]
    },

    resolve: {
        modules: [path.resolve(__dirname, 'src/'), 'node_modules/']
    },

    // externals: {
    //     jquery: 'jQuery'
    // },


    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        // new HtmlWebpackPlugin({
        //     template: __dirname + '/src/template/index.html',
        //     inject: false,
        // }),
        
        new CopyFiles({
            patterns: [
              { from: 'src/img', to: 'img' },
            ],
          }),
        new WebpackBar(),
        new MediaQueryPlugin(),
        // new MiniCssExtractPlugin({
        //     filename: '[name].css',
        //     // chunkFilename: '[id].[contenthash].css',
        // }),

    ]
    .concat(entryHtmlPlugins)
}