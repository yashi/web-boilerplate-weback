const path = require('path');

// Webpack Plugins 
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const MediaQueryPlugin = require('media-query-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyFiles = require('copy-webpack-plugin');
const WebpackBar = require('webpackbar');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const fs = require('fs');
const webpages = JSON.parse(fs.readFileSync('./src/pages.json', 'utf8'));
const filepath = require('./filepath.js');


const argv = require("yargs").argv;
const isProduction = !!argv.production;

const entryHtmlPlugins = webpages.map(function(entryName) {
    return new HtmlWebpackPlugin({
        filename: entryName + '.html',
        template: __dirname + `/src/template/${entryName}.html`,
        inject: false,
        title: entryName,
    })
});

module.exports = {
    mode: 'production',
    devtool: false,
    entry: filepath.inputPath,
    output: {
        path: __dirname + '/dist',
        filename: "bundle.min.js"
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                default: false,
                vendors: false,
                css: {
                    name: 'styles',
                    test: /\.css$/,
                    filename: "/css/[name].min.css",
                    chunks: 'all',
                    // priority: -10,
                    // minChunks: 3,
                    enforce: true
                },
                js: {
                    name: 'vendors',
                    test: /[\\/]node_modules[\\/]/,
                    filename: "/js/[name].min.js",
                    chunks: 'all',
                    // priority: 30,
                    enforce: true
                }
            }
        },
        minimizer: [new TerserPlugin({
            cache: true,
            parallel: true,
            extractComments: true,
            terserOptions: {
                mangle: true, // Note `mangle.properties` is `false` by default.
                output: {
                    comments: false,
                }
            },
        })],

    },
    module: {
        rules: [{
                test: /\.(scss|css)$/,
                exclude: /node_modules/,
                use: [{
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: path.join(__dirname, './dist/', 'assets')
                        }
                    },
                    {
                        loader: "css-loader",
                        options: {
                            // minimize: true,
                            modules: true,
                            importLoaders: 1,
                            url: true,
                        }
                    },

                    {
                        loader: 'postcss-loader',
                        options: {
                            // execute: 'true',
                            postcssOptions: {
                                config: path.resolve(__dirname, 'postcss.config.js'),
                            },
                          },
                    },
                    MediaQueryPlugin.loader,
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: false,
                            implementation: require('node-sass'),
                            sassOptions: {
                                includePaths: [
                                    path.resolve(__dirname, 'node_modules/foundation-sites/scss/'),
                                    path.resolve(__dirname, 'node_modules/bootstrap/scss/')
                                ]
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'mihcm.[hash].[ext]',
                        outputPath: '/css/fonts/',
                        publicPath: 'fonts',
                    },
                }, ],
            },
            {
                test: /\.(gif|png|jpe?g|svg|mp4|webm)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: '/img/',
                        publicPath: '../img/',
                    },
                }, {
                    loader: 'image-webpack-loader',
                    options: {
                        disable: false,
                        mozjpeg: {
                            progressive: true,
                            quality: 65
                        },
                        // optipng: {
                        //     enabled: true,
                        // },
                        pngquant: {
                            quality: '65-90',
                            speed: 4
                        },
                        // gifsicle: {
                        //     interlaced: false,
                        // }
                    },
                }],
            }
        ]
    },

    resolve: {
        modules: [path.resolve(__dirname, 'src/'), 'node_modules/']
    },

    externals: {
        jquery: 'jQuery'
    },

    // performance: {
    //     hints: false
    // },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            
        }),
        new MiniCssExtractPlugin({
            filename: '/css/[name].min.css',
            chunkFilename: '/css/[name].min.css',

        }),
        new WebpackBar(),
        new CopyFiles({
            patterns: [
              { from: 'src/img/icons/', to: 'img' },
            ],
          }),
        new MediaQueryPlugin({
            include: true,
            queries: {
                'screen and (min-width: 0em)': 'small',
                'print, screen and (min-width: 40em)': 'medium',
                'print, screen and (min-width: 64em)': 'large',
                'screen and (min-width: 75em)': 'xlarge',
                'screen and (min-width: 90em)': 'xxlarge'

            }
        }),
        // new OptimizeCssAssetsPlugin({
        //     assetNameRegExp: /\.css$/g,
        //     cssProcessor: require('cssnano'),
        //     cssProcessorPluginOptions: {
        //         preset: ['advanced', {
        //             comments: {
        //                 removeAll: true
        //             }
        //         }],
        //     },
        //     canPrint: true
        // })
    ].concat(entryHtmlPlugins)
}