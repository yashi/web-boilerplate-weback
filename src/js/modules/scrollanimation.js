import ScrollMagic from 'scrollmagic';
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import TweenMax from "gsap/TweenMax";
import SplitText from "./SplitText";
// require("js/modules/SplitText");


const scrollController = new ScrollMagic.Controller({
    addIndicators: false
});


// Scroll Scene

const first_set = new TimelineMax();
let 
    textBox1 = $('.animbox1'),
    anim1 = $('.anim1'),
    readmore = $('.animReadmore1');


let $title = new SplitText('.animTitle1', {
    type: "words,chars"
});
let $description = new SplitText('.animDescription1', {
    type: "words,chars"
});




first_set
    .staggerFrom('.elem1', 1, {
        x: -80,
        autoAlpha: 0,
        ease: Power2.easeInOut,
        stagger:0.5
    })
    .from(textBox1, .5, {
        y: -30,
        autoAlpha: 0,
        ease: Power2.easeInOut,
        // delay:0.2
    }, "+=0.5")
    .staggerFrom($title.chars, .5, {
        y: -10,
        autoAlpha: 0,
        ease: Power4.easeInOut,
        stagger:0.02
    }, "-=4")
    .staggerFrom($description.words, .5, {
        x: 10,
        autoAlpha: 0,
        ease: Power4.easeInOut,
        stagger:0.02
    }, "-=4")
    .from(readmore, .5, {
        y: 30,
        autoAlpha: 0,
        ease: Power2.easeInOut,
    }, "-=2")
    ;



const scene_one = new ScrollMagic.Scene({
    triggerElement: '.trigger_one',
    triggerHook: 0.6,
    reverse:true,
    // duration: 1000,    // the scene should last for a scroll distance of 100px
    // offset: 50
}).setTween(first_set)
.addTo(scrollController);

  
    


    



// 2nd Scroll Scene


var second_set = new TimelineMax();
var 
    textBox2 = $('.animbox2'),
    anim2 = $('.anim2'),
    readmore2 = $('.animReadmore2');


let $title2 = new SplitText('.animTitle2', {
    type: "words,chars"
});
let $description2 = new SplitText('.animDescription2', {
    type: "words,chars"
});




second_set
    .staggerFrom('.elem2', 1, {
        x: 80,
        autoAlpha: 0,
        ease: Power2.easeInOut,
        stagger:0.5
    })
    .from(textBox2, .5, {
        y: 30,
        autoAlpha: 0,
        ease: Power2.easeInOut,
        // delay:0.2
    }, "+=0.5")
    .staggerFrom($title2.chars, .5, {
        y: -10,
        autoAlpha: 0,
        ease: Power4.easeInOut,
        stagger:0.02
    }, "-=4")
    .staggerFrom($description2.words, .5, {
        x: 10,
        autoAlpha: 0,
        ease: Power4.easeInOut,
        stagger:0.02
    }, "-=4")
    .from(readmore2, .5, {
        y: 30,
        autoAlpha: 0,
        ease: Power2.easeInOut,
    }, "-=2")
    ;



const scene_two = new ScrollMagic.Scene({
    triggerElement: '.trigger_two',
    triggerHook: 0.6,
    reverse:true,
    // duration: 1000,    // the scene should last for a scroll distance of 100px
    // offset: 50
}).setTween(second_set)
.addTo(scrollController);




// 3rd Scroll Scene


var third_set = new TimelineMax();
var 
    textBox3 = $('.animbox3'),
    anim3 = $('.anim3'),
    readmore3 = $('.animReadmore3');


let $title3 = new SplitText('.animTitle3', {
    type: "words,chars"
});
let $description3 = new SplitText('.animDescription3', {
    type: "words,chars"
});




third_set
    .staggerFrom('.elem3', 1, {
        x: 80,
        autoAlpha: 0,
        ease: Power2.easeInOut,
        stagger:0.5
    })
    .from(textBox3, .5, {
        y: 30,
        autoAlpha: 0,
        ease: Power2.easeInOut,
        // delay:0.2
    }, "+=0.5")
    .staggerFrom($title3.chars, .5, {
        y: -10,
        autoAlpha: 0,
        ease: Power4.easeInOut,
        stagger:0.02
    }, "-=4")
    .staggerFrom($description3.words, .5, {
        x: 10,
        autoAlpha: 0,
        ease: Power4.easeInOut,
        stagger:0.02
    }, "-=4")
    .from(readmore3, .5, {
        y: 30,
        autoAlpha: 0,
        ease: Power2.easeInOut,
    }, "-=2")
    ;



const scene_three = new ScrollMagic.Scene({
    triggerElement: '.trigger_three',
    triggerHook: 0.6,
    reverse:true,
    // duration: 1000,    // the scene should last for a scroll distance of 100px
    // offset: 50
}).setTween(third_set)
.addTo(scrollController);